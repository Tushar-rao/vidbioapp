import 'package:flutter/material.dart';
import 'package:vidbio/animation/FadeAnimation.dart';
import 'package:vidbio/utils/text_utils.dart';
import 'package:vidbio/widgets/post_view_widget.dart';

class Test extends StatelessWidget {
  final TextUtils _textUtils = TextUtils();
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(left: 10, right: 10, top: 60),
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const CircleAvatar(
                          radius: 60,
                          backgroundImage: NetworkImage(
                              "https://images.unsplash.com/photo-1564564295391-7f24f26f568b")),
                      const SizedBox(height: 5),
                      Container(
                          margin: const EdgeInsets.only(
                            left: 20,
                            right: 10,
                          ),
                          child:
                              _textUtils.normal16("View profile", Colors.white))
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 10, right: 10),
                  // flex: 2,
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(5)),
                    padding: const EdgeInsets.all(3),
                    child: Center(
                      child: _textUtils.normal16(
                          "Share Anything You Want !", Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            child: Row(
              children: [
                Expanded(
                  flex: 8,
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(8)),
                    padding: const EdgeInsets.all(8),
                    child: Center(
                      child:
                          _textUtils.normal16("+ Add Video Link", Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          for (int i = 0; i < 10; i++) const PostViewWidget(),
        ],
      ),
    );
  }
}
