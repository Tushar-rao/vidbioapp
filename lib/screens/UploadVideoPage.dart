import 'package:flutter/material.dart';
import 'package:vidbio/animation/FadeAnimation.dart';
import 'package:vidbio/screens/home.dart';
import 'package:vidbio/screens/Channel_layout.dart';

class UploadVidPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                 
                  Container(
                    margin: const EdgeInsets.only(
                                         top: 30,bottom: 30,left: 20),
                    child: FadeAnimation(
                        1.4,
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              TextButton(
                                  child: Text("Add Channels".toUpperCase(),
                                      style: TextStyle(fontSize: 15)),
                                  style: ButtonStyle(
                                     backgroundColor: MaterialStateProperty.all<Color>(
                                             Color.fromRGBO(255, 185, 8, 1)),
                                      padding:
                                          MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.only(top:20,bottom: 20,left: 30,right: 30)),
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                             Colors.black),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              side: BorderSide(
                                                  color: Colors.black)))),
                                  onPressed: () => null),
                              
                            ])),
                  ),
                  FadeAnimation(
                      1.4,
                     Row(
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: [
                         Container(
                                        margin: const EdgeInsets.only(
                                           top: 20,left: 20),
                                        child: Text(
                                          "Upload Option",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 18),
                                        ),
                                      ),
                       ],
                     ),
                       ),
                  FadeAnimation(
                      1.5,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: [
                              Container(
                                height: 50,
                                width: 150,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10),
                                              topRight: Radius.circular(10),
                                              bottomLeft: Radius.circular(10),
                                              bottomRight: Radius.circular(10)),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Color.fromARGB(134, 115, 194, 226)
                                                      .withOpacity(0.5),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ],
                                        ),
                                        margin: const EdgeInsets.only(
                                             top: 50,left: 30),
                                        child: MaterialButton(
                                          minWidth: double.infinity,
                                          height: 80,
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        HomePage()));
                                          },
                                          child: Container(
                                            child: Row(
                                              children: [
                                                 Icon(
                                                    Icons
                                                        .local_phone ,
                                                    size: 20,
                                                    color: Colors.black,
                                                  ),
                                                Text(
                                                  "Phone",
                                                  style: TextStyle(
                                                      fontWeight: FontWeight.w600,
                                                      fontSize: 18),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                height: 50,
                                width: 150,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10),
                                              topRight: Radius.circular(10),
                                              bottomLeft: Radius.circular(10),
                                              bottomRight: Radius.circular(10)),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Color.fromARGB(133, 226, 115, 115)
                                                      .withOpacity(0.5),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ],
                                        ),
                                        margin: const EdgeInsets.only(
                                             top: 20,left: 30),
                                        child: MaterialButton(
                                          minWidth: double.infinity,
                                          height: 80,
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        HomePage()));
                                          },
                                          child: Container(
                                            child: Row(
                                              children: [
                                                 Icon(
                                                    Icons
                                                        .attach_email ,
                                                    size: 20,
                                                    color: Colors.black,
                                                  ),
                                                Text(
                                                  "Email",
                                                  style: TextStyle(
                                                      fontWeight: FontWeight.w600,
                                                      fontSize: 18),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                            
                            ],
                          ),Column(
                                    children: [
                                      Container(
                            height: 50,
                            width: 150,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10),
                                              topRight: Radius.circular(10),
                                              bottomLeft: Radius.circular(10),
                                              bottomRight: Radius.circular(10)),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Color.fromARGB(133, 207, 115, 226)
                                                      .withOpacity(0.5),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ],
                                        ),
                                        margin: const EdgeInsets.only(
                                            left: 20, top: 50, right: 20),
                                        child: MaterialButton(
                                          minWidth: double.infinity,
                                          height: 80,
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        HomePage()));
                                          },
                                          child: Container(
                                            child: Row(
                                              children: [
                                                 Icon(
                                                    Icons
                                                        .facebook,
                                                    size: 20,
                                                    color: Colors.black,
                                                  ),
                                                Text(
                                                  "Facebook",
                                                  style: TextStyle(
                                                      fontWeight: FontWeight.w600,
                                                      fontSize: 18),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                            height: 50,
                            width: 150,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10),
                                              topRight: Radius.circular(10),
                                              bottomLeft: Radius.circular(10),
                                              bottomRight: Radius.circular(10)),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Color.fromARGB(133, 115, 226, 115)
                                                      .withOpacity(0.5),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ],
                                        ),
                                        margin: const EdgeInsets.only(
                                            left: 20, top: 20, right: 20),
                                        child: MaterialButton(
                                          minWidth: double.infinity,
                                          height: 80,
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        HomePage()));
                                          },
                                          child: Container(
                                            child: Row(
                                              children: [
                                                 Icon(
                                                    Icons
                                                        .sms,
                                                    size: 20,
                                                    color: Colors.black,
                                                  ),
                                                Text(
                                                  "SMS",
                                                  style: TextStyle(
                                                      fontWeight: FontWeight.w600,
                                                      fontSize: 18),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                       
                        ],
                      )),
                ],
              ),
            ),
            // FadeAnimation(
            //     1.2,
            //     Container(
            //       height: MediaQuery.of(context).size.height / 3,
            //       decoration: BoxDecoration(
            //           image: DecorationImage(
            //               image: AssetImage('assets/background.png'),
            //               fit: BoxFit.cover)),
            //     ))
            FadeAnimation(
                1.2,
                  Container(
                    margin:
                        const EdgeInsets.only(bottom: 70),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40),
                          child: Container(
                            padding: EdgeInsets.only(top: 3, left: 3),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border(
                                  bottom: BorderSide(color: Colors.black),
                                  top: BorderSide(color: Colors.black),
                                  left: BorderSide(color: Colors.black),
                                  right: BorderSide(color: Colors.black),
                                )),
                            child: MaterialButton(
                              minWidth: double.infinity,
                              height: 60,
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Channellayout()));
                              },
                              color: Color.fromRGBO(255, 185, 8, 1),
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(100)),
                              child: Text(
                                "Save Changes",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 18),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
          ],
        ),
      ),
    );
  }

  
}
