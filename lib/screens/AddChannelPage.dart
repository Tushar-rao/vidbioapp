import 'package:flutter/material.dart';
import 'package:vidbio/animation/FadeAnimation.dart';
import 'package:vidbio/screens/home_with_post.dart';

class AddChannelPage extends StatelessWidget {
  const AddChannelPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Container(
                    margin:
                        const EdgeInsets.only(top: 10, bottom: 30, right: 30),
                    child: FadeAnimation(
                        1.4,
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                  child: Row(
                                    children: [
                                      Container(
                                        margin: const EdgeInsets.only(
                                            left: 10, right: 10),
                                        child: const Icon(
                                          Icons.remove_red_eye,
                                          size: 30,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text("Preview".toUpperCase(),
                                          style: const TextStyle(fontSize: 15)),
                                    ],
                                  ),
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                      padding:
                                          MaterialStateProperty.all<EdgeInsets>(
                                              const EdgeInsets.only(
                                                  top: 15,
                                                  bottom: 15,
                                                  left: 20,
                                                  right: 20)),
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.white),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              side: const BorderSide(color: Colors.white)))),
                                  // ignore: avoid_returning_null_for_void
                                  onPressed: () => null),
                            ])),
                  ),
                  // FadeAnimation(
                  //     1.4,
                  //     Column(

                  //       children: [
                  //         Container(
                  //           margin: const EdgeInsets.only(left: 30),
                  //           child: const FadeAnimation(
                  //               1.2,
                  //               Text(
                  //                 "Edit Changes",

                  //               )),
                  //         ),
                  //       ],
                  //     )),

                  Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 20, top: 10,bottom: 20),
                          child: Text("Edit Changes".toUpperCase(),
                              style: const TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold)),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 20, top: 10,bottom: 20),
                          child: Text("Change Icon".toUpperCase(),
                              style: const TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                        ),
                        Container(
                           margin: const EdgeInsets.only(left: 20,bottom: 40),
                          child: const CircleAvatar(
                              radius: 60,
                              backgroundImage: NetworkImage(
                                  "https://images.unsplash.com/photo-1564564295391-7f24f26f568b")),
                        ),
                        const SizedBox(height: 5),
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 15,top: 60),
                          height: 60,
                          width: 230,
                          child: Row(
                            children: [
                              Container(
                                 margin: const EdgeInsets.only(right: 15),
                                height: 50,
                                width: 100,
                                child: TextButton(
                                    child: Text("Change".toUpperCase(),
                                        style: const TextStyle(fontSize: 13)),
                                    style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty.all<Color>(
                                            Colors.black),
                                        padding: MaterialStateProperty.all<EdgeInsets>(
                                            const EdgeInsets.only(
                                                top: 10,
                                                bottom: 10,
                                                left: 10,
                                                right: 10)),
                                        foregroundColor: MaterialStateProperty.all<Color>(
                                            Colors.white),
                                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(10),
                                            side: const BorderSide(color: Colors.white)))),
                                    // ignore: avoid_returning_null_for_void
                                    onPressed: () => null),
                              ),
                               Container(
                                height: 50,
                                width: 100,
                                child: TextButton(
                                    child: Text("Remove".toUpperCase(),
                                        style: const TextStyle(fontSize: 13)),
                                    style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty.all<Color>(
                                            const Color.fromARGB(101, 75, 75, 75)),
                                        padding: MaterialStateProperty.all<EdgeInsets>(
                                            const EdgeInsets.only(
                                                top: 10,
                                                bottom: 10,
                                                left: 10,
                                                right: 10)),
                                        foregroundColor: MaterialStateProperty.all<Color>(
                                            Colors.white),
                                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(10),
                                            side: const BorderSide(color: Colors.white)))),
                                    // ignore: avoid_returning_null_for_void
                                    onPressed: () => null),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ]),
                  FadeAnimation(1.2, makeInput(label: "Name")),
                  FadeAnimation(1.3, makeInput(label: "Phone Number")),
                ],
              ),
            ),
            FadeAnimation(
              1.2,
              Container(
                margin: const EdgeInsets.only(bottom: 70),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Container(
                        padding: const EdgeInsets.only(top: 3, left: 3),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: const Border(
                              bottom: BorderSide(color: Colors.black),
                              top: BorderSide(color: Colors.black),
                              left: BorderSide(color: Colors.black),
                              right: BorderSide(color: Colors.black),
                            )),
                        child: MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        Homewithpost()));
                          },
                          color: const Color.fromRGBO(255, 185, 8, 1),
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100)),
                          child: const Text(
                            "Upload Video",
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget makeInput({label, obscureText = false}) {
    return Container(
      margin: const EdgeInsets.only(left: 30, right: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: const TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          const SizedBox(
            height: 5,
          ),
          TextField(
            obscureText: obscureText,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              border: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey)),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
