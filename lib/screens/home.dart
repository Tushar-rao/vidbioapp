import 'package:flutter/material.dart';
import 'package:vidbio/animation/FadeAnimation.dart';

import 'package:vidbio/screens/Video_link_Page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Container(
                    margin:
                        const EdgeInsets.only(top: 30, bottom: 30, left: 20),
                    child: FadeAnimation(
                        1.4,
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const CircleAvatar(
                                        radius: 60,
                                        backgroundImage: NetworkImage(
                                            "https://images.unsplash.com/photo-1564564295391-7f24f26f568b")),
                                    const SizedBox(height: 5),
                                    Container(
                                       margin:
                        const EdgeInsets.only(left: 10,top: 10),
                                      child: Text("View Profile".toUpperCase(),
                                          style: TextStyle(fontSize: 15)),
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  Container(
                                      margin:
                        const EdgeInsets.only(left: 15),
                                    height: 60,
                                    width: 230,
                                    child: TextButton(
                                        child: Text("Share Anything You Want".toUpperCase(),
                                            style: TextStyle(fontSize: 13)),
                                        style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all<Color>(
                                                Colors.black),
                                            padding: MaterialStateProperty.all<EdgeInsets>(
                                                EdgeInsets.only(
                                                    top: 20,
                                                    bottom: 20,
                                                    left: 10,
                                                    right: 10)),
                                            foregroundColor: MaterialStateProperty.all<Color>(
                                                Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10),
                                                side: BorderSide(color: Colors.white)))),
                                        onPressed: () => null),
                                  ),
                                 Text("@_Mr. Rao".toUpperCase(),
                                        style: TextStyle(fontSize: 15)),
                                ],
                                
                              ),
                                  
                                  
                            ])),
                  ),
                 
                  FadeAnimation(
                      1.5,
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                         Image.network('https://cdn.iconscout.com/icon/free/png-256/data-not-found-1965034-1662569.png'),
                         Text("No Post Yet !".toUpperCase(),
                                        style: TextStyle(fontSize: 15, color: Color.fromARGB(122, 105, 105, 103),)),
                         ],
                      )),
                ],
              ),
            ),
            // FadeAnimation(
            //     1.2,
            //     Container(
            //       height: MediaQuery.of(context).size.height / 3,
            //       decoration: BoxDecoration(
            //           image: DecorationImage(
            //               image: AssetImage('assets/background.png'),
            //               fit: BoxFit.cover)),
            //     ))
            FadeAnimation(
              1.2,
              Container(
                margin: const EdgeInsets.only(bottom: 70),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      child: Container(
                        padding: EdgeInsets.only(top: 3, left: 3),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: Border(
                              bottom: BorderSide(color: Colors.black),
                              top: BorderSide(color: Colors.black),
                              left: BorderSide(color: Colors.black),
                              right: BorderSide(color: Colors.black),
                            )),
                        child: MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Video_link_page()));
                          },
                          color: Color.fromRGBO(255, 185, 8, 1),
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100)),
                          child: Text(
                            "+ Add Video Link",
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  
}
