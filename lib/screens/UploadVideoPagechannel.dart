import 'package:flutter/material.dart';
import 'package:vidbio/animation/FadeAnimation.dart';
import 'package:vidbio/screens/AddChannelPage.dart';

class Uploadvideopagechannel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  // Column(
                  //   mainAxisAlignment: MainAxisAlignment.start,
                  //   crossAxisAlignment: CrossAxisAlignment.start,
                  //   children: <Widget>[
                  //     FadeAnimation(
                  //         1,
                  //         Text(
                  //           "Select Video Type",
                  //           style: TextStyle(
                  //               fontSize: 30, fontWeight: FontWeight.bold),
                  //         )),
                  //     SizedBox(
                  //       height: 20,
                  //     ),

                  //   ],
                  // ),
                  // Container(
                  //   margin:
                  //       const EdgeInsets.only(top: 10, bottom: 30, right: 30),
                  //   child: FadeAnimation(
                  //       1.4,
                  //       Row(
                  //           mainAxisAlignment: MainAxisAlignment.end,
                  //           children: [
                  //             TextButton(
                  //                 child: Row(
                  //                   children: [
                  //                     Container(
                  //                       margin: const EdgeInsets.only(
                  //                           left: 10, right: 10),
                  //                       child: Icon(
                  //                         Icons.remove_red_eye,
                  //                         size: 30,
                  //                         color: Colors.white,
                  //                       ),
                  //                     ),
                  //                     Text("Preview".toUpperCase(),
                  //                         style: TextStyle(fontSize: 15)),
                  //                   ],
                  //                 ),
                  //                 style: ButtonStyle(
                  //                     backgroundColor:
                  //                         MaterialStateProperty.all<Color>(
                  //                             Colors.black),
                  //                     padding:
                  //                         MaterialStateProperty.all<EdgeInsets>(
                  //                             EdgeInsets.only(
                  //                                 top: 15,
                  //                                 bottom: 15,
                  //                                 left: 20,
                  //                                 right: 20)),
                  //                     foregroundColor:
                  //                         MaterialStateProperty.all<Color>(
                  //                             Colors.white),
                  //                     shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  //                         RoundedRectangleBorder(
                  //                             borderRadius:
                  //                                 BorderRadius.circular(20),
                  //                             side: BorderSide(color: Colors.white)))),
                  //                 onPressed: () => null),
                  //           ])),
                  // ),
                  
              

                  FadeAnimation(
                      1.5,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.all(15),
                            height: screenHeight * 0.5,
                            width: screenWidth * 0.9,
                            // color: Colors.black,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                  bottomLeft: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 3,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  FadeAnimation(
                      1.4,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 30,top:30),
                            child: FadeAnimation(
                                1.2,
                                Text(
                                  "Selected Channels ",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                )),
                          ),
                        ],
                      )),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 30,),
                                    child: TextButton(
                                      
                                        child: Text("Phone".toUpperCase(),
                                            style: TextStyle(fontSize: 12)),
                                        style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all<Color>(
                                                Color.fromARGB(255, 95, 233, 252)),
                                            padding: MaterialStateProperty.all<EdgeInsets>(
                                                EdgeInsets.only(
                                                    top: 10,
                                                    bottom: 10,
                                                    left: 20,
                                                    right: 20)),
                                            foregroundColor:
                                                MaterialStateProperty.all<Color>(
                                                    Colors.black),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50), side: BorderSide(color: Colors.black)))),
                                        onPressed: () => null),
                                  ),
                                  Container(
                                    height: 180,
                                    width: 150,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                              Color.fromARGB(134, 115, 194, 226)
                                                  .withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(
                                        left: 20, top: 30),
                                    child: MaterialButton(
                                      minWidth: double.infinity,
                                      height: 80,
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    AddChannelPage()));
                                      },
                                      child: Container(
                                        child: Row(
                                          children: [
                                            Text(
                                              "Record Video",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 18),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ]),
                          )
                        ],
                      )),
                ],
              ),
            ),
            FadeAnimation(
                1.2,
                  Container(
                    margin:
                        const EdgeInsets.only(bottom: 70),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40),
                          child: Container(
                            padding: EdgeInsets.only(top: 3, left: 3),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border(
                                  bottom: BorderSide(color: Colors.black),
                                  top: BorderSide(color: Colors.black),
                                  left: BorderSide(color: Colors.black),
                                  right: BorderSide(color: Colors.black),
                                )),
                            child: MaterialButton(
                              minWidth: double.infinity,
                              height: 60,
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AddChannelPage()));
                              },
                              color: Color.fromRGBO(255, 185, 8, 1),
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(100)),
                              child: Text(
                                "Save Changes",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 18),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
          ],
        ),
      ),
    );
  }

}
