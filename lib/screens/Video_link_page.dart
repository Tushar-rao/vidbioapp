import 'package:flutter/material.dart';
import 'package:vidbio/animation/FadeAnimation.dart';
import 'package:vidbio/screens/home.dart';
import 'package:vidbio/screens/UploadVideoPage.dart';

class Video_link_page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      FadeAnimation(
                          1,
                          Text(
                            "Select Video Type",
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      // Container(
                      //   child: FadeAnimation(
                      //       1.2,
                      //       Text(
                      //         "Welcome Home ",
                      //         style: TextStyle(
                      //             fontSize: 15, color: Colors.grey[700]),
                      //       )),
                      // ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                                         top: 30,bottom: 30),
                    child: FadeAnimation(
                        1.4,
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              TextButton(
                                  child: Text("Single Video Link".toUpperCase(),
                                      style: TextStyle(fontSize: 15)),
                                  style: ButtonStyle(
                                     backgroundColor: MaterialStateProperty.all<Color>(
                                             Color.fromRGBO(255, 185, 8, 1)),
                                      padding:
                                          MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.only(top:20,bottom: 20,left: 10,right: 10)),
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                             Colors.black),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              side: BorderSide(
                                                  color: Colors.black)))),
                                  onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UploadVidPage()));
                          }),
                              SizedBox(width: 10),
                              TextButton(
                                  child: Text("Multiple Video Link".toUpperCase(),
                                      style: TextStyle(fontSize: 15)),
                                  style: ButtonStyle(
                                     backgroundColor: MaterialStateProperty.all<Color>(
                                             Color.fromARGB(255, 252, 252, 252)),
                                      padding:
                                          MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.only(top:20,bottom: 20,left: 10,right: 10)),
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                             Colors.black),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              side: BorderSide(
                                                  color: Colors.black)))),
                                  onPressed: () => null),
                            ])),
                  ),
                  // FadeAnimation(
                  //     1.4,
                  //     Column(
                  //       mainAxisAlignment: MainAxisAlignment.start,
                  //       children: [
                  //         Padding(
                  //           padding: EdgeInsets.symmetric(horizontal: 40),
                  //           child: Container(
                  //             padding: EdgeInsets.only(top: 3, left: 3),
                  //             decoration: BoxDecoration(
                  //                 borderRadius: BorderRadius.circular(50),
                  //                 border: Border(
                  //                   bottom: BorderSide(color: Colors.black),
                  //                   top: BorderSide(color: Colors.black),
                  //                   left: BorderSide(color: Colors.black),
                  //                   right: BorderSide(color: Colors.black),
                  //                 )),
                  //             child: MaterialButton(
                  //               minWidth: double.infinity,
                  //               height: 60,
                  //               onPressed: () {
                  //                 Navigator.push(
                  //                     context,
                  //                     MaterialPageRoute(
                  //                         builder: (context) => HomePage()));
                  //               },
                  //               color: Color.fromRGBO(255, 185, 8, 1),
                  //               elevation: 0,
                  //               shape: RoundedRectangleBorder(
                  //                   borderRadius: BorderRadius.circular(100)),
                  //               child: Text(
                  //                 "Login",
                  //                 style: TextStyle(
                  //                     fontWeight: FontWeight.w600,
                  //                     fontSize: 18),
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  //       ],
                  //     )),
                  FadeAnimation(
                      1.5,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.all(15),
                            height: screenHeight * 0.5,
                            width: screenWidth * 0.9,
                            // color: Colors.black,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                  bottomLeft: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 3,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 20, top: 40),
                                    child: Text(
                                      "Upload Option",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18),
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                              Color.fromARGB(134, 115, 194, 226)
                                                  .withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(
                                        left: 20, top: 50, right: 20),
                                    child: MaterialButton(
                                      minWidth: double.infinity,
                                      height: 80,
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    HomePage()));
                                      },
                                      child: Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(20),
                                              decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    255, 185, 8, 0.35),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(50)),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.white,
                                                    spreadRadius: 5,
                                                    blurRadius: 7,
                                                    offset: Offset(0,
                                                        3), // changes position of shadow
                                                  ),
                                                ],
                                              ),
                                              margin: const EdgeInsets.only(
                                                  right: 20),
                                              child: Icon(
                                                Icons
                                                    .fiber_manual_record_outlined,
                                                size: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                            Text(
                                              "Record Video",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 18),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                              Color.fromARGB(133, 115, 126, 226)
                                                  .withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(
                                        left: 20, top: 20, right: 20),
                                    child: MaterialButton(
                                      minWidth: double.infinity,
                                      height: 80,
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    HomePage()));
                                      },
                                      child: Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(15),
                                              decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    255, 185, 8, 0.35),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(50)),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.white,
                                                    spreadRadius: 5,
                                                    blurRadius: 7,
                                                    offset: Offset(0,
                                                        3), // changes position of shadow
                                                  ),
                                                ],
                                              ),
                                              margin: const EdgeInsets.only(
                                                  right: 20),
                                              child: Icon(
                                                Icons.file_upload_rounded,
                                                size: 30,
                                                color: Colors.black,
                                              ),
                                            ),
                                            Text(
                                              "Upload Video",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 18),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                              Color.fromARGB(133, 124, 226, 115)
                                                  .withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(
                                        left: 20, top: 20, right: 20),
                                    child: MaterialButton(
                                      minWidth: double.infinity,
                                      height: 80,
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    HomePage()));
                                      },
                                      child: Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(15),
                                              decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    255, 185, 8, 0.35),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(50)),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.white,
                                                    spreadRadius: 5,
                                                    blurRadius: 7,
                                                    offset: Offset(0,
                                                        3), // changes position of shadow
                                                  ),
                                                ],
                                              ),
                                              margin: const EdgeInsets.only(
                                                  right: 20),
                                              child: Icon(
                                                Icons.folder_shared_rounded,
                                                size: 30,
                                                color: Colors.black,
                                              ),
                                            ),
                                            Text(
                                              "Select From Video Library",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 15),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ]),
                          )
                        ],
                      )),
                ],
              ),
            ),
            // FadeAnimation(
            //     1.2,
            //     Container(
            //       height: MediaQuery.of(context).size.height / 3,
            //       decoration: BoxDecoration(
            //           image: DecorationImage(
            //               image: AssetImage('assets/background.png'),
            //               fit: BoxFit.cover)),
            //     ))
          ],
        ),
      ),
    );
  }

  Widget makeInput({label, obscureText = false}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          label,
          style: TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
        ),
        SizedBox(
          height: 5,
        ),
        TextField(
          obscureText: obscureText,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(50),
            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey)),
          ),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
