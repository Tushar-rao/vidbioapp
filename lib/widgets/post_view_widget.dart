import 'package:flutter/material.dart';
import 'package:vidbio/utils/text_utils.dart';

class PostViewWidget extends StatefulWidget {
  const PostViewWidget({Key? key}) : super(key: key);

  @override
  _PostViewWidgetState createState() => _PostViewWidgetState();
}

class _PostViewWidgetState extends State<PostViewWidget> {
  final TextUtils _textUtils = TextUtils();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    const CircleAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage(
                          "https://images.unsplash.com/photo-1564564295391-7f24f26f568b"),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    _textUtils.bold16("Date - 01/12/2050", Colors.white)
                  ],
                ),
                const Icon(
                  Icons.more_vert_rounded,
                  color: Colors.white,
                )
              ],
            ),
          ),
          Image.asset(
            "assets/dummy_post.jpg",
            height: 300,
            fit: BoxFit.fill,
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
